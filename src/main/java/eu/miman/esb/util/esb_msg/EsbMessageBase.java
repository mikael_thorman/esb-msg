package eu.miman.esb.util.esb_msg;

public interface EsbMessageBase {

	/**
	 * The name of the message on the ESB.
	 * @return The name of the message on the ESB.
	 */
	String messageName();
	
	/**
	 * The version of this message.
	 * @return The version of this message..
	 */
	String messageVersion();
}
