package eu.miman.esb.util.esb_msg;

/**
 * This enum is used to inform about which type of CRUD modification that was performed on an object.
 *  
 * @author Mikael Thorman
 */
public enum TypeOfModification {
	CREATED,
	UPDATED,
	DELETED;
}
